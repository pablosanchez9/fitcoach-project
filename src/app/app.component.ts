import { Component, ViewChild } from '@angular/core';
import { FCM } from '@ionic-native/fcm';
import { Platform, NavController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireAuth } from 'angularfire2/auth';


import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('myNav') navCtrl: NavController; 
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, afAuth: AngularFireAuth, fcm: FCM, public toastCtrl: ToastController) {
    

    

    const authUnsubscribe = afAuth.authState.subscribe( user => { 
      if (user != null) { this.rootPage = HomePage; } 
      else { this.rootPage = 'LoginPage'; } 
    }); 

    platform.ready().then(() => {

      
      fcm.onNotification().subscribe( data => {
         if(data.wasTapped){ 
          this.presentToast("1", data.clientId); 
          //Notification was received on device tray and tapped by the user. 
           //console.log( JSON.stringify(data) ); 
           //this.navCtrl.setRoot('ClientDetailPage', { 'clientId': data.clientId}); 
           this.navCtrl.push('ClientDetailPage', { clientId: data.clientId});
          }
           else{ 
            this.presentToast("2", data.clientId);
             //Notification was received in foreground. Maybe the user needs to be notified. 
             //console.log( JSON.stringify(data)); 
             this.navCtrl.push('ClientCreatePage');
             //this.navCtrl.push('ClientDetailPage', { clientId: data.clientId});
            } 
          }); 

          fcm.onTokenRefresh().subscribe(token=>{            
            this.presentToast("refresh",fcm.getToken() +"-"+ token);
          })
      
    

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

  }

  presentToast(s, t) {
    let toast = this.toastCtrl.create({
      message: 'notificacion recibida: ' + s + " - " + t,
      duration: 3000
    });
    toast.present();
  }
}

