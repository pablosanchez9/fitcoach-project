const functions = require('firebase-functions');
const admin = require('firebase-admin'); 
admin.initializeApp(functions.config().firebase); 

exports.sendWeightUpdate = functions.database 
.ref('/userProfile/{userId}/weightTrack/{weightId}') 
.onWrite(event => { 
    const clientId = event.params.userId; 
    const weight = event.data.val().weight; 
    return admin.database().ref(`/userProfile/${clientId}/`) 
    .once('value', clientProfileSnapshot => { 
        const coachId = clientProfileSnapshot.val().coachId; 
        const clientName = clientProfileSnapshot.val().fullName; 
        const clientStartingWeight = clientProfileSnapshot.val().startingWeight; 
        return admin.database().ref(`/userProfile/${coachId}/`).once('value', 
        profileSnapshot => { 
            // Notification details. 
            const payload = { 
                "notification":{ 
                    "title": `${clientName} just shared a weight update`, 
                    "body": `${clientName} started at ${clientStartingWeight} and just updated to ${weight}`, 
                    "sound":"default", 
                    "click_action":"FCM_PLUGIN_ACTIVITY" 
                }, 
                "data":{ "clientId": clientId } 
            }; 
            console.log(payload);
            return admin.messaging().sendToDevice(clientProfileSnapshot.val().token, payload)
            .then(function(response) {
                // See the MessagingTopicManagementResponse reference documentation
                // for the contents of response.
                console.log("Enviado corrrectamente:", response);
              })
              .catch(function(error) {
                console.log("Error al enviar:", error);
              });
            console.log("mensaje enviado");
        }); 
    }); 
});



exports.createLog = functions.https.onRequest((req,res) => {
    console.log("Creando mi primer log");
        var query = admin.database().ref("/userProfile/").orderByKey();
        query.once("value")
        .then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                //var key = childSnapshot.val().fullName; // "ada"
                console.log("fullName: " + childSnapshot.val().fullName);  
                console.log("startingWeight: " + childSnapshot.val().startingWeight);  
                console.log("token: " + childSnapshot.val().token);  
                
                childSnapshot.child("weightTrack").forEach(function (d) {
                    console.log(d.val().date);
                    console.log(d.val().weight);
                  });                
            });
        });
      res.status(200).send("OK");                
}); 

